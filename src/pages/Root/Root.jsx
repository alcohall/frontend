import React from 'react';

import Block from 'components/Block';
import BlockContent from 'components/BlockContent';
import BlockContentTitle from "../../components/BlockContentTitle";
import Logo from "../../components/Logo";
import Line from "../../components/Line";

const Root = () => {
  return (
    <>
      <Block part="top" />
      <Block part="middle" />

      <Block part="content">
        <BlockContent>
          <Logo/>
          <Line>Проект по подбору рецептов коктейлей по имеющимся ингредиентам.</Line>
          <Line>Ключевые особенности:</Line>
          <Line>• Список любимых рецептов</Line>
          <Line>• Выбор близких по рецептуре коктейлей</Line>
          <Line>• Отображение всех характеристик коктейлей</Line>
          <Line>• Особенные подборки и фильтрация по обстановке</Line>
          <Line>• Добавление своих рецептов</Line>
          <Line>• Личный кабинет</Line>
        </BlockContent>
      </Block>

      <Block part="content">
        <BlockContent>
          <BlockContentTitle>Lorem ipsum dolor sit amet</BlockContentTitle>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Facilisis
          sed odio morbi quis commodo odio. Et sollicitudin ac orci phasellus
          egestas tellus. Dapibus ultrices in iaculis nunc sed. Varius morbi
          enim nunc faucibus a. Vitae elementum curabitur vitae nunc sed velit.
          Volutpat commodo sed egestas egestas fringilla phasellus faucibus
          scelerisque. Dui vivamus arcu felis bibendum ut tristique et egestas
          quis. Gravida neque convallis a cras semper. Habitasse platea dictumst
          quisque sagittis purus sit amet. Auctor eu augue ut lectus. Arcu non
          sodales neque sodales ut etiam sit. Aliquam ultrices sagittis orci a
          scelerisque purus semper eget. Volutpat maecenas volutpat blandit
          aliquam etiam. Penatibus et magnis dis parturient montes nascetur
          ridiculus mus mauris. Morbi non arcu risus quis varius quam quisque
          id. Ac turpis egestas maecenas pharetra convallis posuere. Praesent
          tristique magna sit amet purus gravida quis blandit turpis. Sed augue
          lacus viverra vitae congue eu consequat ac. Sed risus pretium quam
          vulputate dignissim suspendisse. Cursus mattis molestie a iaculis at
          erat pellentesque adipiscing commodo. Elementum nibh tellus molestie
          nunc non blandit massa enim nec. Eu lobortis elementum nibh tellus. In
          pellentesque massa placerat duis. Consectetur adipiscing elit ut
          aliquam purus sit amet luctus. Aliquam eleifend mi in nulla.
        </BlockContent>
      </Block>

      <Block part="content" />

      <Block part="bottom" />
    </>
  );
};

export default Root;
